#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
}

Widget::~Widget()
{
    delete ui;
}

void Widget::on_pushButton_clicked()
{
    QStringList vals = ui->lineEdit->text().split(',');
    short N = vals.length();
    short M=0;
    int Nums[N];
    int Max = vals[0].toInt();
    for(short i=0; i<N; ++i) {
        vals[i] = vals[i].trimmed();
        if (vals[i]!="") Nums[i] = vals[i].toInt();
        else Nums[i] = 0;

        int CurNum = vals[i].toInt();
        if (CurNum > Max) {
            Max = CurNum;
            M=i;
        }
        qDebug()<<CurNum<<" "<<Max;
    }
    ui->lineEdit->clear();
    for(short i=0; i<N; ++i) {
//        qDebug()<<Nums[i];
//        qDebug()<<i<<"!="<<M;
        if (i!=M) {
            qDebug()<<i<<"!="<<M<<Nums[i];
            QString CurText = ui->lineEdit->text();
            ui->lineEdit->setText(CurText+QString::number(Nums[i]));
            if (i<(N-1)){
                CurText = ui->lineEdit->text();
                ui->lineEdit->setText(CurText+", ");
            }
        }
    }
}
